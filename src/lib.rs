use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub struct ClientRequest {
    pub process_id: u8,
    pub processed_row: i16,
    pub max_found: i64,
}

pub struct SquareMatrix(Vec<Vec<i64>>);

impl SquareMatrix {
    pub fn new(size: usize) -> SquareMatrix {
        let mut vec = vec![];
        for i in 0..size {
            let mut row = vec![];
            for j in 0..size {
                row.push((i + j) as i64);
            }
            vec.push(row);
        }
        SquareMatrix(vec)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn get(&self, i: usize, j: usize) -> Option<i64> {
        if i > self.0.len() || j > self.0.len() {
            return None;
        }
        let x = self.0.get(i).unwrap().get(j).unwrap();
        Some(x.clone())
    }
}
