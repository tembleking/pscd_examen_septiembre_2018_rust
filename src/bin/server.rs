use pscd::ClientRequest;
use std::error::Error;
use std::net::{TcpListener, TcpStream};

use std::sync::{Arc, Mutex};
use std::thread::spawn;

fn handle_client(stream: TcpStream, max: Arc<Mutex<i64>>) -> Result<(), Box<dyn Error>> {
    loop {
        let res = bincode::deserialize_from(&stream);
        if res.is_err() {
            // Client disconnected
            return Ok(());
        }
        let result: ClientRequest = res.unwrap();
        let mut existing_max = max.lock().unwrap();
        if *existing_max < result.max_found {
            *existing_max = result.max_found;
        }
        println!("{:#?}", result);
    }
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("0.0.0.0:5000")?;

    let mut handlers = vec![];
    let mut num_clients = 0;
    let max = Arc::new(Mutex::new(-9223372036854775808i64)); // min i64 number

    for stream in listener.incoming() {
        let arc = max.clone();
        let handle = spawn(|| {
            handle_client(stream.unwrap(), arc).unwrap();
        });
        handlers.push(handle);

        num_clients += 1;
        if num_clients >= 3 {
            // we will only handle 3 connections in this code
            break;
        }
    }

    handlers.into_iter().for_each(|h| {
        h.join().unwrap();
    });

    println!("Max found is: {}", max.lock().unwrap());
    Ok(())
}
