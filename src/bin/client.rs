use pscd::{ClientRequest, SquareMatrix};
use std::error::Error;

use std::net::TcpStream;
use std::sync::{Arc, Mutex};

fn process(
    id: u8,
    url: &str,
    matrix: Arc<SquareMatrix>,
    row: Arc<Mutex<i16>>,
) -> Result<(), Box<dyn Error>> {
    let conn = TcpStream::connect(url)?;
    loop {
        let next_row = {
            let mut row = row.lock().unwrap();
            let next_row = *row;
            *row -= 1;
            next_row
        };

        if next_row < 0 {
            return Ok(());
        }

        let mut local_max = matrix.get(next_row as usize, 0).unwrap();
        for i in 0..matrix.len() {
            let found = matrix.get(next_row as usize, i).unwrap();
            if local_max < found {
                local_max = found;
            }
        }

        bincode::serialize_into(
            &conn,
            &ClientRequest {
                process_id: id,
                processed_row: next_row,
                max_found: local_max,
            },
        )?;
    }
}

fn main() -> std::io::Result<()> {
    let matrix = Arc::new(SquareMatrix::new(10));
    let rows = Arc::new(Mutex::new((matrix.len() - 1) as i16));

    let address = "localhost:5000";
    let mut handlers = vec![];

    for i in 0..3 {
        let matrix_cloned = matrix.clone();
        let rows_cloned = rows.clone();
        let handle = std::thread::spawn(move || {
            process(i, address, matrix_cloned, rows_cloned).unwrap();
        });
        handlers.push(handle);
    }

    handlers.into_iter().for_each(|h| {
        h.join().unwrap();
    });

    Ok(())
}
